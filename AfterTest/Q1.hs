data Tree a = Leaf a
              | Node a (Tree a) (Tree a)

sumTC (Leaf x) k = k x
sumTC (Node a left right) k = sumTC left contleft
    where contleft sleft = sumTC right contright
              where contright sright = k (a + sleft + sright)

sumTC (Node 3 (Leaf 4) (Leaf 2)) id
  sumTC (Leaf 4) (\sleft -> sumTC (Leaf 2) (\sright -> id (3 + sleft + sright)))
   (\sleft -> sumTC (Leaf 2) (\sright -> id (3 + sleft + sright))) (4)
   sumTC (Leaf 2) (\sright -> id (3 + 4 + sright)))
   (\sright -> id (3 + 4 + sright))) (2) 
  id (3 + 4 + sright) 2
  id (3 + 4 + 2) 
  id (9)
  9

sumTC2 (Leaf x) k = k x
sumTC2 (Node a left right) k = 
       sumTC2 left (\sleft -> sumTC2 right (\sright -> k (a + sleft + sright)))
  
sumTC2 (Node 3 (Leaf 3) (Leaf 2)) id =   
  sumTC2 (Leaf 3) (\sleft -> sumTC2 right (\sright -> id (3 + sleft + sright)))
  (\sleft -> sumTC2 (Leaf 2) (\sright -> id (3 + sleft + sright))) (3)
  sumTC2 (Leaf 2) (\sright -> id (3 + 3 + sright))
  (\sright -> id (3 + 3 + sright)) (2)
  id (3 + 3 + 2)
  id (8)
  8
