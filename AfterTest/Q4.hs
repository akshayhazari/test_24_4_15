import Data.List

subseq :: Ord a => [a] -> [a]
subseq xs = foldl (\acc x -> if (length (c x)) > length (acc) then c x else acc) [] (tails xs)
        where c z 
                | (length z) <=1 = z  
                | otherwise = let hd = head z in (hd: filter (>hd) z)
                     

remdA :: Eq a =>  [a] -> [a]                                          
         
remdA xs = concat $ filter (\x -> (length x) == 1) (group xs)   

remdA1 :: Eq a => [a] -> [a]
remdA1 [] = [] 
remdA1 (x:xs) = if length(ln x xs) > 0 then remdA1 (dropWhile (==x) xs) else (x:(remdA1 xs))
  where ln y ys = takeWhile (==y) ys 


remd :: Eq a => [a] -> [a]
remd xs = nub xs

remd1 :: Eq a =>  [a] -> [a]
remd1 [x] = [x]
remd1 (x:xs) = (x:(remd1 (filter (/=x) xs))) 